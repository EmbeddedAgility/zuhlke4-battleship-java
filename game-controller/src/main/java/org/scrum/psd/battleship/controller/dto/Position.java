package org.scrum.psd.battleship.controller.dto;

public class Position {

    private Letter column;
    private int row;
    private boolean hit;

    public Position() {
        super();
    }

    @Override
    public String toString() {
        return "Position{" +
                "column=" + column +
                ", row=" + row +
                '}';
    }

    public Position(Letter column, int row) {
        this();

        this.column = column;
        this.row = row;
        this.hit = false;
    }

    public Letter getColumn() {
        return column;
    }

    public void setColumn(Letter column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public boolean isHit() {
        return this.hit;
    }

    public void hit() {
        this.hit = true;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Position) {
            Position position = (Position) o;

            if (position == null) {
                return false;
            }

            return position.column.equals(column) && position.row == row;
        }

        return false;
    }
}
