package org.scrum.psd.battleship.controller;

import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;

public class GameController {
    public static boolean checkIsHit(Collection<Ship> ships, Position shot) {
        List<Ship> listOfShips = new ArrayList<>(ships);

        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : listOfShips) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {
                    ship.removePosition(position);
                    if (ship.getPositions().size() == 0) {
                        sunkShip(ship);
                        listOfShips.remove(ship);
                        System.out.println(ship.getName() + " sunk!");
                    }
                    return true;
                }
            }
        }

        return false;
    }

    public static List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 2, Color.CADET_BLUE),
                new Ship("Battleship", 2, Color.RED)
               /* new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE)*/);
    }

    public static boolean isShipValid(Ship ship) {
        return ship.getPositions().size() == ship.getSize();
    }

    public static Position getRandomPosition(int size) {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(size)];
        int number = random.nextInt(size);
        Position position = new Position(letter, number);
        return position;
    }

    public static void sunkShip(Ship ship) {
        ship.setSunk(true);
    }

    public static boolean checkIfFleetIsDestroyed(Collection<Ship> ships) {

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (!position.isHit()) {
                    return false;
                }
            }
        }

        return true;
    }
}
