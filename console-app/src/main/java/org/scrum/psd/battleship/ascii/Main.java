package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static List<Position> enemyShots;
    private static ColoredPrinter console;
    private static RetTypes retTypes = null;

    public static void main(String[] args) {
        console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        System.out.println(Utils.SPACER);
        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.setForegroundColor(Ansi.FColor.WHITE);
        Utils.promptEnterKey("start the game");
        System.out.println(Utils.SPACER);
        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(Utils.SPACER);
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");
        console.println("Ships positioned successfully! Game is about to start!");
        Utils.promptEnterKey("continue");
        System.out.println(Utils.SPACER);
        do {
            System.out.println(Utils.SPACER);
            console.println("Player, it's your turn");
            Utils.printInputMessage("Enter coordinates for your shot :");
            Position position;
            do {
                position = parsePosition(scanner.next());
                if (position == null && retTypes == RetTypes.OUTSIDE_FIELD) {
                    System.out.println();
                    Utils.printNegativeMessage("Miss!");
                    Utils.printInputMessage("Position outside the field, try again:");
                } else if (position == null && retTypes == RetTypes.INCORRECT_INPUT) {
                    console.println("First input is one letter A-F and second is number 1-8");
                }
            } while (position == null);
            System.out.println(Utils.SPACER);
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                beep();
                printHitASCII();
                Utils.printPositiveMessage("Yeah ! Nice hit !");
            } else {
                printMissASCII();
                Utils.printNegativeMessage("Miss");
            }

            if (GameController.checkIfFleetIsDestroyed(enemyFleet)) {
                console.println("░██████╗░░██████╗░");
                console.println("██╔════╝░██╔════╝░");
                console.println("██║░░██╗░██║░░██╗░");
                console.println("██║░░╚██╗██║░░╚██╗");
                console.println("╚██████╔╝╚██████╔╝");
                console.println("░╚═════╝░░╚═════╝░");
                console.println(" ");
                console.println("You are the winner!");
                break;
            }

            console.println("Computer shooting next!");
            Utils.promptEnterKey("continue");

            position = getComputerShot();
            System.out.println(Utils.SPACER);
            isHit = GameController.checkIsHit(myFleet, position);

            if (isHit) {
                beep();
                printHitASCII();
            } else {
                printMissASCII();
            }
            String computerTurnMessage = String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), isHit ? "hit your ship !" : "miss");
            if (isHit) Utils.printNegativeMessage(computerTurnMessage);
            else Utils.printPositiveMessage(computerTurnMessage);
            Utils.promptEnterKey("play your turn");
            if (GameController.checkIfFleetIsDestroyed(myFleet)) {
                Utils.printNegativeMessage("You lost!");
                break;
            }
        } while (true);
    }

    private static void printMissASCII() {
        for (int i = 0; i < 4; i++) {
            Utils.printColoredMessage("_      _      _      _      _     ", Ansi.FColor.BLUE);
            Utils.printColoredMessage(")`'-.,_)`'-.,_)`'-.,_)`'-.,_)`'-.,_", Ansi.FColor.BLUE);
        }
    }

    private static void printHitASCII() {
        Utils.printColoredMessage("              \\        .  ./", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("               \\  .:\" \";'.:..\" \"   /", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("                  (M^^.^~~:.'\" \").", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("            -   (/  .    . . \\ \\)  -", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("               ((| :. ~ ^  :. .|))", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("            -   (\\- |  \\ /  |  /)  -", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("                 -\\  \\     /  /-", Ansi.FColor.YELLOW);
        Utils.printColoredMessage("                   \\  \\   /  /", Ansi.FColor.YELLOW);
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        Character characterInput;
        Letter letter;
        int number;
        try {
            characterInput = input.toUpperCase().substring(0, 1).charAt(0);
            if (!Character.isAlphabetic(characterInput)) {
                retTypes = RetTypes.INCORRECT_INPUT;
            }
            letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
            number = Integer.parseInt(input.substring(1));

        } catch (Exception e) {
            retTypes = RetTypes.OUTSIDE_FIELD;
            return null;
        }
        if (number < 1 || number > 8) {
            retTypes = RetTypes.OUTSIDE_FIELD;
            return null;
        }
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        return new Position(letter, number);
    }

    public static Position getComputerShot() {
        Position shotPosition;
        boolean repeatedShot = true;
        do {
            shotPosition = getRandomPosition();
            if(!enemyShots.contains(shotPosition)) repeatedShot = false;
            else enemyShots.add(shotPosition);
        }
        while(repeatedShot);
        return shotPosition;
    }

    private static void InitializeGame() {
        enemyShots = new ArrayList<>();
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        Utils.printColoredMessage(
                "Please position your fleet (Game board has size from A to H and 1 to 8)",
                Ansi.FColor.MAGENTA);
        for (Ship ship : myFleet) {
            System.out.println("");
            console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                Utils.printInputMessage(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));
                String positionInput = scanner.next();
                ship.addPosition(positionInput);
            }
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        List<Ship> currentFleet = new LinkedList<>();
        for(Ship ship : enemyFleet){
            generatePosition(ship, currentFleet);
        }
        enemyFleet = currentFleet;
    }

    private static int columnToInt(Letter letter){
        switch(letter){

            case A:
                return 0;
            case B:
                return 1;
            case C:
                return 2;
            case D:
                return 3;
            case E:
                return 4;
            case F:
                return 5;
            case G:
                return 6;
            case H:
                return 7;
            default:
                return 0;
        }
    }

    private static Letter intToColumn(int num){
        switch (num){
            case 0:
                return Letter.A;
            case 1:
                return Letter.B;
            case 2:
                return Letter.C;
            case 3:
                return Letter.D;
            case 4:
                return Letter.E;
            case 5:
                return Letter.F;
            case 6:
                return Letter.G;
            case 7:
                return Letter.H;
            default:
                return null;

        }
    }

    private static void generatePosition(Ship ship, List<Ship> currentFleet) {
        Position position;
        List<Position> unavailablePositions = new LinkedList<>();

        for(Ship shipCurrent : currentFleet){
            unavailablePositions.addAll(shipCurrent.getPositions());
        }
        boolean isCreated = false;
        while(!isCreated) {
            while (true) {
                position = getRandomPosition();
                if (!unavailablePositions.contains(position)) {
                    break;
                }
            }


            for (int dir = 0; dir < 4; dir++) {
                List<Position> currentShipPositions = new LinkedList<>();
                currentShipPositions.add(position);

                if (dir == 0) {
                    boolean canFit = true;
                    //0 is up
                    for (int i = 1; i < ship.getSize(); i++) {
                        int newRow = position.getRow() - i;
                        if (newRow < 0) {
                            canFit = false;
                            break;
                        }
                        Position newPosition = new Position(position.getColumn(), newRow);
                        if (unavailablePositions.contains(newPosition)) {
                            break;
                        }
                        currentShipPositions.add(newPosition);
                    }
                    if (canFit) {
                        ship.getPositions().addAll(currentShipPositions);
                        currentFleet.add(ship);
                        isCreated = true;
                        break;
                    }
                } else if (dir == 1) {
                    boolean canFit = true;
                    for (int i = 1; i < ship.getSize(); i++) {
                        int newColumn = columnToInt(position.getColumn()) + i;
                        if (newColumn > 7) {
                            canFit = false;
                            break;
                        }
                        Position newPosition = new Position(intToColumn(newColumn), position.getRow());
                        if (unavailablePositions.contains(newPosition)) {
                            break;
                        }
                        currentShipPositions.add(newPosition);
                    }
                    if (canFit) {
                        ship.getPositions().addAll(currentShipPositions);
                        currentFleet.add(ship);
                        isCreated = true;
                        break;
                    }
                } else if (dir == 2) {
                    boolean canFit = true;
                    for (int i = 1; i < ship.getSize(); i++) {
                        int newRow = position.getRow() + i;
                        if (newRow > 7) {
                            canFit = false;
                            break;
                        }
                        Position newPosition = new Position(position.getColumn(), newRow);
                        if (unavailablePositions.contains(newPosition)) {
                            break;
                        }
                        currentShipPositions.add(newPosition);
                    }
                    if (canFit) {
                        ship.getPositions().addAll(currentShipPositions);
                        currentFleet.add(ship);
                        isCreated = true;
                        break;
                    }
                } else {
                    boolean canFit = true;
                    //3 is left
                    for (int i = 1; i < ship.getSize(); i++) {
                        int newColumn = columnToInt(position.getColumn()) - i;
                        if (newColumn < 0) {
                            canFit = false;
                            break;
                        }
                        Position newPosition = new Position(intToColumn(newColumn), position.getRow());
                        if (unavailablePositions.contains(newPosition)) {
                            break;
                        }
                        currentShipPositions.add(newPosition);
                    }
                    if (canFit) {
                        ship.getPositions().addAll(currentShipPositions);
                        currentFleet.add(ship);
                        isCreated = true;
                        break;
                    }
                }
            }
        }

    }
}
