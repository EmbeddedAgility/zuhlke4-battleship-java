package org.scrum.psd.battleship.ascii;
import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;

import java.util.Scanner;

public class Utils {
    private static final Scanner stdIn = new Scanner(System.in);
    public static final String SPACER = "\n\n\n\n\n\n\n\n\n\n\n\n\n";
    public static final ColoredPrinter console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

    public static void promptEnterKey(String message) {
        printInputMessage("Press \"ENTER\" to " + message);
        stdIn.nextLine();
    }

    public static void printColoredMessage(String message, Ansi.FColor color) {
        console.setForegroundColor(color);
        console.println(message);
        console.setForegroundColor(Ansi.FColor.WHITE);
    }

    public static void printInputMessage(String message) {
        printColoredMessage(message, Ansi.FColor.BLUE);
    }

    public static void printPositiveMessage(String message) {
        printColoredMessage(message, Ansi.FColor.GREEN);
    }

    public static void printNegativeMessage(String message) {
        printColoredMessage(message, Ansi.FColor.RED);
    }
}
